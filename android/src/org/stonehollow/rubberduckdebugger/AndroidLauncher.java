package org.stonehollow.rubberduckdebugger;

import android.os.Bundle;
import android.widget.RelativeLayout;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import org.stonehollow.rubberduckdebugger.RubberDuckDebugger;

public class AndroidLauncher extends AndroidApplication {
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		config.useImmersiveMode = false;
		config.hideStatusBar = false;
		RelativeLayout layout = new RelativeLayout(this);
		layout.addView(initializeForView(new RubberDuckDebugger(), config));
		setContentView(layout);
		//initialize(new RubberDuckDebugger(), config);
	}
}
