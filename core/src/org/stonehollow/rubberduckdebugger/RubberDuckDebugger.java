package org.stonehollow.rubberduckdebugger;


import com.badlogic.gdx.Game;

public class RubberDuckDebugger extends Game {

	@Override
	public void create() {
		this.showMainScreen();
	}

	public void showMainScreen(){
		setScreen(new MainScreen(this));
	}
}
