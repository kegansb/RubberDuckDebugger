package org.stonehollow.rubberduckdebugger;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.ExtendViewport;

import static com.badlogic.gdx.Gdx.input;
import static com.badlogic.gdx.utils.TimeUtils.nanoTime;
import static com.badlogic.gdx.utils.TimeUtils.nanosToMillis;

/**
 * Created by luna on 8/31/17.
 */

public class MainScreen extends InputAdapter implements Screen {

    ExtendViewport viewport;
    SpriteBatch batch;
    Texture duckTexture;
    Sprite duckSprite;
    //Texture helpTexture;
    //Sprite helpSprite;
    TextFrame frame;
    Array<String> suggestions;

    float lastTime;
    float appeared;

    public MainScreen(Game game){

    }

    public void show(){
        appeared = nanoTime();
        viewport = new ExtendViewport(2000f,2000f);
        batch = new SpriteBatch();
        duckTexture = new Texture("duck.png");
        duckSprite = new Sprite(duckTexture);
        //helpTexture = new Texture("?.png");
        //helpSprite = new Sprite(helpTexture);
        frame = new TextFrame();
        frame.visible = true;
        frame.print("Explain your code to me.");
        suggestions = new Array<String>();
        suggestions.add("Maybe you forgot to declare a variable?");
        suggestions.add("Don't forget: = != ==");
        suggestions.add("Is there a brace in the wrong place?");
        suggestions.add("Could a function be receiving an unexpected argument?");
        suggestions.add("Hmm...");
        suggestions.add("Tell me more...");
        suggestions.add("Can you elaborate on how that works?");
        suggestions.add("Did you forget a semicolon?");
        suggestions.add("Off by one error?");
        suggestions.add("I don't understand.");
        suggestions.add("Don't give up!");
        lastTime = nanoTime();
    }

    public void pause(){

    }

    public void resume(){

    }

    public void hide(){

    }

    public void resize(int x, int y){
        viewport.update(x,y,true);
        frame.dimensions = new Vector2(viewport.getWorldWidth(),viewport.getWorldHeight()/3);
    }

    public void render(float delta){
        if(input.justTouched()){
            Vector2 touchPosition = viewport.unproject(new Vector2(input.getX(),input.getY()));
            /*
            if(touchPosition.x > helpSprite.getX()){
                if(touchPosition.y > helpSprite.getY()){
                    //\\Gdx.net.openURI("https://en.wikipedia.org/wiki/Rubber_duck_debugging");
                    //System.out.println("Opening Browser");
                }
            }
            */
            if(frame.printFinished){
                frame.visible = false;
            }else{
                frame.proceed();
            }

        }

        if(nanosToMillis((long)(nanoTime() - appeared)) > 5000){
            frame.visible = false;
        }

        if(nanosToMillis((long)(nanoTime() - lastTime)) > 10000){
            lastTime = nanoTime();
            this.newSpeech();
        }

        // Clear the screen
        Gdx.gl.glClearColor(.3f, .3f, .4f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // Position the duck
        if(viewport.getWorldWidth() < viewport.getWorldHeight()){
            duckSprite.setScale(viewport.getWorldWidth() / duckTexture.getWidth());
        }else{
            duckSprite.setScale(viewport.getWorldHeight() / duckTexture.getHeight());
        }
        duckSprite.setX(viewport.getWorldWidth()/2 - duckSprite.getWidth()/2);
        duckSprite.setY(viewport.getWorldHeight()/2 - duckSprite.getHeight()/2);

        // Position help button
        //helpSprite.setScale(viewport.getWorldHeight() / helpTexture.getHeight()/8);
        //helpSprite.setY(viewport.getWorldHeight() - helpSprite.getHeight()*helpSprite.getScaleY());
        //helpSprite.setX(viewport.getWorldWidth() - helpSprite.getWidth()*helpSprite.getScaleX());

        // Draw all the things
        batch.setProjectionMatrix(viewport.getCamera().combined);
        batch.begin();
        duckSprite.draw(batch);
        //helpSprite.draw(batch);
        frame.render(batch,delta);
        batch.end();
    }

    public void newSpeech(){
        appeared = nanoTime();
        frame.visible = true;
        frame.print(suggestions.random());
    }

    public void dispose(){
        batch.dispose();
        duckTexture.dispose();
        //helpTexture.dispose();
    }
}
